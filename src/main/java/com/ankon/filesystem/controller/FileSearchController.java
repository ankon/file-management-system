package com.ankon.filesystem.controller;

import com.ankon.filesystem.domain.dto.SearchDTO;
import com.ankon.filesystem.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class FileSearchController {

    private FileService fileService;

    @Autowired
    public FileSearchController(FileService fileService) {
        this.fileService = fileService;
    }

    @GetMapping("/search-file")
    public String searchForm(Model model) {
        List<String> searchResult = new ArrayList<>();
        model.addAttribute("searchResult", searchResult);

        model.addAttribute("search", new SearchDTO(""));
        return "file_search";
    }

    @PostMapping("/search-file")
    public String submitSearch(@ModelAttribute SearchDTO search, Model model) {
        List<String> searchResult = fileService.searchByKeyword(search.getKeyword());
        if (searchResult == null) {
            searchResult = new ArrayList<>();
        }
        model.addAttribute("searchResult", searchResult);

        model.addAttribute("search", search);

        return "file_search";
    }

}
