package com.ankon.filesystem.service;

import com.ankon.filesystem.common.exception.UserNotFoundException;
import com.ankon.filesystem.domain.User;

public interface UserService {

    User readByUsername(String username) throws UserNotFoundException;

    boolean isPasswordMatches(User user, String password) throws Exception;

}