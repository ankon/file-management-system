package com.ankon.filesystem.service.mapping;

import com.ankon.filesystem.common.exception.EntityNotFoundException;
import com.ankon.filesystem.common.util.PasswordUtil;
import com.ankon.filesystem.domain.Role;
import com.ankon.filesystem.domain.User;
import com.ankon.filesystem.domain.dto.UserDTO;
import com.ankon.filesystem.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.ankon.filesystem.common.util.PasswordUtil.EncType.BCRYPT_ENCODER;

@Service
public class UserMapper {

    @Autowired
    private RoleService roleService;

    /**
     * Maps DTO to Entity
     *
     * @param dto
     * @return entity
     */
    public User map(UserDTO dto) throws Exception {

        Role role = roleService.findRoleById(dto.getRoleId());
        if (role == null) {
            throw new EntityNotFoundException("Role does not exist");
        }
        User entity = new User();
        entity.setName(dto.getName());
        entity.setDesignation(dto.getDesignation());
        entity.setUsername(dto.getUsername());
        entity.setPassword(PasswordUtil.encryptPassword(dto.getPassword(), BCRYPT_ENCODER, null));
        entity.setPhone(dto.getPhone());
        entity.setRole(role);

        return entity;
    }

}