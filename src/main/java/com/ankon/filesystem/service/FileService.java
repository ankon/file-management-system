package com.ankon.filesystem.service;

import com.ankon.filesystem.domain.FilePath;
import com.ankon.filesystem.repository.FilePathRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class FileService {

    FilePathRepository filePathRepository;

    @Autowired
    public FileService(FilePathRepository filePathRepository) {
        this.filePathRepository = filePathRepository;
    }

    private String readFromInputStream(InputStream inputStream)
            throws IOException {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br
                     = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        }
        return resultStringBuilder.toString();
    }

    public void readFileStructure() {
        ClassLoader classLoader = getClass().getClassLoader();

        InputStream inputStream = null;
        try {
            File file = new File(classLoader.getResource("fileStructure.txt").getFile());
            inputStream = new FileInputStream(file);

            String data = readFromInputStream(inputStream);

            String[] fileStructure = tokenizeFileStructure(data);

            if (fileStructure == null || fileStructure.length == 0) {
                return;
            }

            generateFileTree(fileStructure);

            List<FilePath> filePaths = filePathRepository.findAllBy();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private String[] tokenizeFileStructure(String files) {
        if (files == null || files.length() == 0)
            return null;

        return files.split("\n");
    }

    private void generateFileTree(String[] fileStructure) {
        filePathRepository.deleteAll();

        if (fileStructure == null || fileStructure.length == 0)
            return;

        long parent = -1L;
        int pathToRootCount = 0;
        List<FilePath> parentTree = new ArrayList<>();

        for (int i = 0; i < fileStructure.length; i++) {
            pathToRootCount = StringUtils.countOccurrencesOf(fileStructure[i], "\t");

            if (pathToRootCount == 0) {
                parent = -1;
            } else {
                parent = parentTree.get(pathToRootCount - 1).getId();
            }

            try {
                FilePath filePath = new FilePath();

                String fileName = fileStructure[i].replaceAll("\t", "");
                filePath.setName(fileName);

                if (parent == -1) {
                    filePath.setPath(fileStructure[i]);
                } else {
                    FilePath parentFile = filePathRepository.findFirstById(parent);
                    filePath.setPath((parentFile.getPath() + "\\" + fileName).replaceAll("\\\\\\\\", "\\\\"));
                }

                filePath.setSlug(filePath.getPath().toLowerCase());

                filePath.setParentId(parent);

                if (fileName.contains(".")) filePath.setFile(true);

                filePath = filePathRepository.save(filePath);

                if (parent == -1) {
                    parentTree = new ArrayList<>();
                }

                if (!filePath.getFile()) {
                    if (parentTree.size() > pathToRootCount)
                        parentTree.add(pathToRootCount, filePath);
                    else parentTree.add(filePath);
                }
            } catch (Exception e) {
                e.printStackTrace();
                break;
            }
        }
    }

    public List<String> searchByKeyword(String keyword) {
        return filePathRepository.searchByKeyword(keyword.toLowerCase());
    }

}
