package com.ankon.filesystem.service;

import com.ankon.filesystem.domain.Role;

public interface RoleService {

    Role findRoleById(long id);

}
