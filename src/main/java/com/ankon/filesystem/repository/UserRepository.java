package com.ankon.filesystem.repository;

import com.ankon.filesystem.domain.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

    User findByUsername(String username);

}