package com.ankon.filesystem.repository;

import com.ankon.filesystem.domain.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Long> {

    Role findById(long id);

}
