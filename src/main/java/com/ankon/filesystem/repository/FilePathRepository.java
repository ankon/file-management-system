package com.ankon.filesystem.repository;

import com.ankon.filesystem.domain.FilePath;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FilePathRepository extends CrudRepository<FilePath, Long> {

    FilePath findFirstById(Long id);
    List<FilePath> findAllBy();

    @Query(value = "SELECT path FROM file_paths f WHERE f.slug LIKE CONCAT('%', :keyword, '%') ORDER BY f.id ASC", nativeQuery = true)
    List<String> searchByKeyword(@Param("keyword") String keyword);

}
