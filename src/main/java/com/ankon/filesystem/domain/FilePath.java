package com.ankon.filesystem.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "file_paths")
public class FilePath extends BaseEntity {

    private String name;

    private Long parentId = -1L;

    private String path = "";

    private String slug = "";

    private Boolean isFile = false;

    public FilePath() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Boolean getFile() {
        return isFile;
    }

    public void setFile(Boolean file) {
        isFile = file;
    }

    @Override
    public String toString() {
        return "FilePath{" +
                "name='" + name + '\'' +
                ", parentId=" + parentId +
                ", path='" + path + '\'' +
                ", slug='" + slug + '\'' +
                ", isFile=" + isFile +
                '}';
    }
}
