package com.ankon.filesystem.common.exception;

public class NullPasswordException extends NullPointerException{
    public NullPasswordException(String s) {
        super(s);
    }
}